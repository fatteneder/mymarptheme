---
marp        : true
paginate    : true
theme       : mystyle
---      
 
![bg left:33%](https://wikiimg.tojsiabtv.com/wikipedia/commons/thumb/b/bc/Socrate_du_Louvre.jpg/440px-Socrate_du_Louvre.jpg)
<!-- _class: titlepage -->

<!-- # Graph Algebra Representation of Formally Defined Programs in Z -->
<!-- Using Haskell to Reason and Verify Programs -->
<!-- Leonard Kleinrock -->
<!-- 01.jan.2022 -->
<!-- Formal Methods International Congress -->

<div class="title">My Marp template presentation</div>
<div class="subtitle">Layout and color scheme adjusted to my likings</div>
<div class="author">Florian Atteneder</div>
<div class="date">January, 2022</div>
<div class="organization">TPI Jena</div>

---

# Hello world

Tahis is my presentation now.

---

# Good day
<!-- _class: white-background -->

---

# Good bye
