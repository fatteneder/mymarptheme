# My Marp setup

## Installation

### Marp

Download a binary from https://github.com/marp-team/marp-cli/releases and unpack it.

Add directory of `marp` executable to `PATH` variable.

### Themes

```
git clone https://gitlab.com/fatteneder/mymarptheme.git
```

## Usage

Launch marp with
```
marp --html --theme-set mymarp/ -s . &
```

Then go to your browser and open `localhost:8080`.

## TODOs

Things to improve:
- Make headers stay on top while content is centered on page.
  This must be changed within the theme/.css file I think.
  The following did the trick:
  ```
  section {
    justify-content: flex-start;
  }
  ```
  See [here](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container)
  for an explanation.
- Custom commands for LaTeX mode?
  Definition:
  ```
  $$
  {\gdef\commandname{<replacement-text>}}
  $$
  ```
  Usage:
  ```
  $$
  \commandname
  $$
  ```

- How to hide a slide?
  Might be useful to use a hidden slide to define custom KaTeX commands.
  A feature request can be found [here](https://github.com/marp-team/marp/discussions/162), although the motivation behind that is different.
  Not sure if that is actually needed or if I can't just sumggle in a `$$ ... $$` block into one
  of the first slides.

- Align equations in LaTeX mode?
  Works with
  ```
   $$
   \begin{align*}
      a &= b + c
      \\
      &= e + f
   \end{align*}
   $$
  ```
  See [KaTeX Docs](https://katex.org/docs/supported.html) for all supported functions.

- Add a rectangular box as a footer in which a short title and page number are contained.
  The coloring should be blue on the left and orange on the right side, similar
  to [here](https://github.com/TAdeJong/beamer-universiteit-leiden)
  (I think) this is not doable with CSS only. It might be doable by inserting an HTML tag
  at the end with a box that is dressed with a linear gradient from left to right.
  However, Marp does not allow for arbitrary HTML insertion, hence, not doable alone with
  CSS. The closest we can come to is to a background-image to section that has a 
  linear gradient that runs from top to bottom, e.g.
  ```
  background-image: linear-gradient(180deg, 
                        var(--unijena-white) 91%, 
                        var(--unijena-orange) 91%, 
                        var(--unijena-orange) 92%,
                        var(--unijena-blue) 92%, 
                        var(--unijena-blue) 100%);
  ```

- Clean up CSS files.

# Credits

Most of the CSS is based on https://github.com/cunhapaulo/marpstyle
